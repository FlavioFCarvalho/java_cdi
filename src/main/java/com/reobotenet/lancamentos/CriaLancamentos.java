package com.reobotenet.lancamentos;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.reobotenet.model.Lancamento;
import com.reobotenet.model.Pessoa;
import com.reobotenet.model.TipoLancamento;
import com.reobotenet.util.JpaUtil;

public class CriaLancamentos {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		EntityManager manager = JpaUtil.getEntityManager();
		EntityTransaction trx = manager.getTransaction();
		trx.begin();
		
		Calendar dataVencimento1 = Calendar.getInstance();
		dataVencimento1.set(2013,10, 1, 0, 0, 0);
		
		Calendar dataVencimento2 = Calendar.getInstance();
		dataVencimento2.set(2013,12, 10, 0, 0, 0);
		
		Pessoa cliente = new Pessoa();
		cliente.setNome("WWW indústria da Alimentos");
		
		Pessoa fornecedor = new Pessoa();
		fornecedor.setNome("SoftBRAX Treinamentos");
		
		Lancamento lancamento1 = new Lancamento();
	lancamento1.setDescricao("Venda de licença de software");
		lancamento1.setPessoa(cliente);
		lancamento1.setDataVencimento(dataVencimento1.getTime());
		lancamento1.setDataPagamento(dataVencimento1.getTime());
		lancamento1.setValor(new BigDecimal(103_100));
		lancamento1.setTipo(TipoLancamento.RECEITA);
		
		
		Lancamento lancamento2 = new Lancamento();
		lancamento2.setDescricao("Treinamento da equipe");
		lancamento2.setPessoa(fornecedor);
		lancamento2.setDataVencimento(dataVencimento2.getTime());
		lancamento2.setValor(new BigDecimal(68_000));
		lancamento2.setTipo(TipoLancamento.DESPESA);
		
		manager.persist(cliente);
		manager.persist(fornecedor);
		manager.persist(lancamento1);
		manager.persist(lancamento2);
		
		trx.commit();
		manager.close();
		
	}

}
