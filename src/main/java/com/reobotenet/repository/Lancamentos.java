package com.reobotenet.repository;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.reobotenet.model.Lancamento;

public class Lancamentos {

	private EntityManager manager;

	@Inject
	public Lancamentos(EntityManager manager) {
		this.manager = manager;
	}

	// public void adicionar(Lancamento lancamento) {
	// EntityTransaction trx = this.manager.getTransaction();
	// trx.begin();
	// this.manager.persist(lancamento);
	// trx.commit();
	// }

	// public void adicionar(Lancamento lancamento ){
	// this.manager.persist(lancamento);
	// }

	public Lancamento porId(Long id) {
		return manager.find(Lancamento.class, id);
	}

	public Lancamento guardar(Lancamento lancamento) {
		return this.manager.merge(lancamento);
	}

	public List<Lancamento> todos() {

		TypedQuery<Lancamento> query = manager.createQuery("from Lancamento",
				Lancamento.class);
		return query.getResultList();

	}
	
	public void remover(Lancamento lancamento){
		this.manager.remove(lancamento);
	}

	public List<String> descricaoQueContem(String descricao) {

		TypedQuery<String> query = manager.createQuery(
				"select distinct descricao from lancamento "
						+ "where upper (descricao) like upper (:descricao)",
				String.class);
		query.setParameter("descricao", "%" + descricao + "%");

		return query.getResultList();
	}

}
