package com.reobotenet.service;

import java.util.Date;

import javax.inject.Inject;

import com.reobotenet.model.Lancamento;
import com.reobotenet.repository.Lancamentos;
import com.reobotenet.util.Transactional;

/*Nossa classe de negócio que trata de cadastro de lançamentos. Essa
 classe dependerá do respositório de lançamentos, por isso, ela deve receber o objeto
 do repositório no construtor.*/

public class CadastroLancamentos {

	@Inject
	private Lancamentos lancamentos;

	// public CadastroLancamentos(Lancamentos lancamentos) {
	//
	// this.lancamentos = lancamentos;
	// }

	@Transactional
	public void salvar(Lancamento lancamento) throws NegocioException {
		if (lancamento.getDataPagamento() != null
				&& lancamento.getDataPagamento().after(new Date())) {

			throw new NegocioException(
					"Data de pagamento não pode ser uma data futura.");
		}

		this.lancamentos.guardar(lancamento);
		System.out.println("Cadastro service!");
	}
    @Transactional
	public void excluir(Lancamento lancamento) throws NegocioException{
    	
    	lancamento = this.lancamentos.porId(lancamento.getId());
    	
    	if(lancamento.getDataPagamento() != null){
    		throw new NegocioException("Não é possível excluir um lançamento pago!");
    	}
    	
    	this.lancamentos.remover(lancamento);
		
	}

}
