package com.reobotenet.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.reobotenet.model.Lancamento;
import com.reobotenet.model.Pessoa;
import com.reobotenet.model.TipoLancamento;
import com.reobotenet.repository.Lancamentos;
import com.reobotenet.repository.Pessoas;
import com.reobotenet.service.CadastroLancamentos;
import com.reobotenet.service.NegocioException;
import com.reobotenet.util.JpaUtil;

@Named
@javax.faces.view.ViewScoped
public class CadastroLancamentoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroLancamentos cadastro;

	// private List<Pessoa> todasPessoas;

	@Inject
	private Pessoas pessoas;

	@Inject
	private Lancamentos lancamentos;

	private Lancamento lancamento = new Lancamento();
	private List<Pessoa> todasPessoas;

	public List<String> pesquisaDescricoes(String descricao) {
		return this.lancamentos.descricaoQueContem(descricao);

	}

	// public void prepararCadastro(){
	//
	// EntityManager manager = JpaUtil.getEntityManager();
	// try {
	//
	// Pessoas pessoas = new Pessoas(manager);
	// this.todasPessoas = pessoas.todas();
	//
	// } finally {
	// manager.close();
	// }
	// System.out.println("Abrindo a conexão");
	// }

	public void prepararCadastro() {
		this.todasPessoas = this.pessoas.todas();
		if (this.lancamento == null) {
			this.lancamento = new Lancamento();
		}
	}

	// public void salvar(){
	//
	// // EntityManager manager = JpaUtil.getEntityManager();
	// // EntityTransaction trx = manager.getTransaction();
	//
	// FacesContext context = FacesContext.getCurrentInstance();
	//
	// try{
	// trx.begin();
	// CadastroLancamentos cadastro = new CadastroLancamentos(
	// new Lancamentos(manager));
	// cadastro.salvar(this.lancamento);
	//
	// this.lancamento = new Lancamento();
	// context.addMessage(null, new FacesMessage(
	// "Lançamento salvo com sucesso!") );
	// System.out.println("Salvo");
	// trx.commit();
	// }catch(NegocioException e){
	// trx.rollback();
	// FacesMessage mensagem = new FacesMessage(e.getMessage());
	// mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
	//
	// }finally{
	// manager.close();
	// }
	// }

	public void salvar() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.salvar(this.lancamento);

			this.lancamento = new Lancamento();
			context.addMessage(null, new FacesMessage(
					"Lançamento salvo com sucesso!"));
		} catch (NegocioException e) {

			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	
	

	public List<Pessoa> getTodasPessoas() {
		return this.todasPessoas;
	}

	public TipoLancamento[] getTiposLancamentos() {
		return TipoLancamento.values();
	}

	public Lancamento getLancamento() {
		return lancamento;

	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}

}
