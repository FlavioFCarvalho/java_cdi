package com.reobotenet.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.reobotenet.model.Lancamento;
import com.reobotenet.repository.Lancamentos;

import com.reobotenet.util.JpaUtil;


import com.reobotenet.service.CadastroLancamentos;
import com.reobotenet.service.NegocioException;


@Named
@ViewScoped
public class ConsultaLancamentoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Lancamentos lancamentosRepository;

	@Inject
	private CadastroLancamentos cadastro;

	private List<Lancamento> lancamentos;

	private Lancamento lancamentoSelecionado;

	
	public void consultar() {
		this.lancamentos = lancamentosRepository.todos(); 
	}
	
	public void excluir(){
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			
			this.cadastro.excluir(this.lancamentoSelecionado);
			this.consultar();
			
			context.addMessage(null, new FacesMessage(
					"Lançamento excluído com sucesso"));
			
		} catch (NegocioException e) {
			
			FacesMessage mensagem = new FacesMessage(e.getLocalizedMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
			
		}
	}

	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public Lancamento getLancamentoSelecionado() {
		return lancamentoSelecionado;
	}

	public void setLancamentoSelecionado(Lancamento lancamentoSelecionado) {
		this.lancamentoSelecionado = lancamentoSelecionado;
	}

}